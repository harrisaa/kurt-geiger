const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const postcssNormalize = require('postcss-normalize')

const config = require('./webpack.common.js')

config.mode = 'production'

config.module = {
  rules: [
    {
      test: /\.scss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            modules: true,
            localIdentName: '[hash:base64:5]'
          }
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
            plugins: () => [
              postcssNormalize(/* pluginOptions */)
            ]
          }
        }
      ]
    },
    {
      test: /\.(ttf|eot|svg|png|woff2?|jpe?g)?(\?.+)?$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 100000,
            name: '[name].[ext]'
          }
        }
      ]
    },
    {
      test: /\.jsx?$/,
      enforce: 'post',
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env'],
            ['@babel/preset-react', {'development': false}]
          ]
        }
      }
    }
  ],
  noParse: /\.min\.js/
}

module.exports = config
