const webpack = require('webpack')
const path = require('path')

const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const WriteFilePlugin = require('write-file-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const Clean = require('clean-webpack-plugin')

module.exports = {
  target: 'web',
  cache: true,
  devtool: 'source-map',
  context: path.resolve(__dirname),
  entry: ['babel-polyfill', 'react-hot-loader/patch', 'src/index.jsx'],
  output: {
    path: path.join(__dirname, '/build/static'),
    publicPath: '/',
    filename: 'client.[hash].js',
    chunkFilename: '[name].[id].js',
    sourceMapFilename: '[name].js.map'
  },
  devServer: {
    contentBase: './build',
    hotOnly: true,
    hot: true,
    port: 3000
  },
  resolve: {
    extensions: ['*', '.js', '.jsx', '.scss'],
    alias: {
      Image: path.resolve(__dirname, 'src/assets/image/'),
      Style: path.resolve(__dirname, 'src/assets/Stylesheet/')
    },
    modules: [
      path.resolve(__dirname),
      'node_modules'
    ]
  },
  plugins: [
    new Clean([path.join(__dirname, './build')]),
    new webpack.DefinePlugin({
      'process.env': {
        //NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        NODE_ENV: JSON.stringify(path.resolve(__dirname))
      }
    }),
    new HtmlWebpackPlugin({
      template: './src/index.html',
      favicon: './src/assets/image/dices.svg',
      inject: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.LoaderOptionsPlugin({debug: true}),
    new WriteFilePlugin({test: /\.html$/, useHashIndex: true}),
    new MiniCssExtractPlugin({
      filename: '[name].[hash].css',
      allChunks: true
    })
  ],
  optimization: {
    namedModules: true, // NamedModulesPlugin()
    splitChunks: { // CommonsChunkPlugin()
      name: 'vendor',
      minChunks: 2
    },
    noEmitOnErrors: true, // NoEmitOnErrorsPlugin
    concatenateModules: true //ModuleConcatenationPlugin
  }
}
