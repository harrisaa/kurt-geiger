import React from 'react'
import { Route, Switch } from 'react-router-dom'

import { page } from 'src/defaults'

import Home from 'src/App/page/Home'
import Instructions from 'src/App/page/Instructions'
import Header from 'src/App/component/Header'
import Footer from 'src/App/component/Footer'
import NotFound from 'src/App/page/PageNotFound'
import Layout from 'src/App/component/Layout'

const routes = [
  {
    path: page.home.url,
    exact: true,
    main: Home
  },
  {
    path: page.instructions.url,
    exact: true,
    main: Instructions
  },
  {
    path: '/*',
    exact: true,
    main: NotFound
  },
  {
    path: page.notFound.url,
    exact: true,
    main: NotFound
  }
]

const Routes = () => {
  return (
    <Layout>
      <Switch>
        {routes.map((route, index) => (
          <Route key={index} path={route.path} strict={false} exact={route.exact} component={Header}/>
        ))}
      </Switch>
      <Switch>
        {routes.map((route, index) => (
          <Route key={index} path={route.path} strict={false} exact={route.exact} component={route.main}/>
        ))}
      </Switch>
      <Switch>
        {routes.map((route, index) => (
          <Route key={index} path={route.path} strict={false} exact={route.exact} component={Footer}/>
        ))}
      </Switch>
    </Layout>
  )
}

export default Routes
