import React, { Component } from 'react'

import StyleProvider from 'src/App/component/StyleProvider'
import Routes from 'src/Route'

class App extends Component {

  render () {
    return (
      <StyleProvider {...this.props}>
        <Routes/>
      </StyleProvider>
    )
  }
}

export default App
