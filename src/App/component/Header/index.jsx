import React, { Component } from 'react'

import Link from 'src/App/component/Header/Link'
import {page} from 'src/defaults'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/Header/index.scss'

export class Header extends Component {
  render () {
    return (
      <header className="header__container">
        <h1 className={style.head}>
          Kurt Geiger
        </h1>
        <ul className={style.list}>
          <li><Link to={page.home.url}>Game</Link></li>
          <li><Link to={page.instructions.url}>Instructions</Link></li>
        </ul>
      </header>
    )
  }
}

export default withStyles(style)(Header)
