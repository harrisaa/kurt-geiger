import React from 'react'
import { NavLink } from 'react-router-dom'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/Header/index.scss'

export default withStyles(style)((props) => <NavLink to exact className={style.link} activeClassName={style.active} {...props} />)
