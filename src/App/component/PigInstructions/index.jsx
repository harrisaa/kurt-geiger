import React, { Component } from 'react'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/PigInstructions/index.scss'

export class PigInstructions extends Component {
  render () {

    return (

      <div className={style.container}>
        <h2 className={style.head}>Pig Dice Game Rules</h2>
        <p>Pig is a simple dice game which in its basic form is playable with just a single die. You win by being the
          first player to score 100 or more points.</p>
        <p>To play you'll need 2 to 10 players, one 6-sided dice, and a pencil and some paper for keeping score.</p>
        <p>Skull dice (where the 1 is replaced with a skull) work well for this game.</p>

        <h2 className={style.head}>So how do you play Pig?</h2>
        <p>Choose a player to go first. That player throws a die and scores as many points as the total shown on the die
          providing the die doesn’t roll a 1. The player may continue rolling and accumulating points (but risk rolling
          a 1) or end his turn.</p>
        <p>If the player rolls a 1 his turn is over, he loses all points he accumulated that turn, and he passes the die
          to the next player.
        </p>
        <p>Play passes from player to player until a winner is determined.</p>

        <h2 className={style.head}>How do you win?</h2>
        <p>The first player to accumulate 100 or more points wins the game. </p>
      </div>
    )
  }
}

export default withStyles(style)(PigInstructions)
