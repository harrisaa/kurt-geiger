import React, { Component } from 'react'

import PropTypes from 'prop-types'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/Username/index.scss'

export class Username extends Component {
  static propTypes = {
    onChange: PropTypes.func.isRequired
  }

  render () {
    const {onChange} = this.props

    return (
      <fieldset className={style.container}>
        <div className={style.item}>
          <label htmlFor="playerOneName">Player 1</label>
          <input type="text" id="playerOneName" placeholder="Name" onChange={onChange} aria-required="true" required/>
        </div>
        <div className={style.item}>
          <label htmlFor="playerTwoName">Player 2</label>
          <input type="text" id="playerTwoName" placeholder="Name" onChange={onChange} aria-required="true" required/>
        </div>
      </fieldset>
    )
  }
}

export default withStyles(style)(Username)
