import React, { Component } from 'react'
import PropTypes from 'prop-types'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/ButtonPanel/index.scss'

export class PlayerForm extends Component {
   static propTypes = {
    passTurn: PropTypes.func.isRequired,
    rollDice: PropTypes.func.isRequired,
  }

  render () {
    const { passTurn, rollDice} = this.props

    return (
      <div className={style.panel}>
        <button id="dice" onClick={rollDice} aria-disabled="true" className={style.dice}>Roll Dice</button>
        <button id="pass" onClick={passTurn} aria-disabled="true">Pass Turn</button>
      </div>
    )
  }
}

export default withStyles(style)(PlayerForm)
