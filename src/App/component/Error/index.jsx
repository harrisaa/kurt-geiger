import React, { Component } from 'react'

class Error extends Component {

  render () {
    return <h1>Something went wrong.</h1>
  }
}

export default Error
