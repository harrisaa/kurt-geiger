import { Component, Children } from 'react'
import PropTypes from 'prop-types'

export default class StyleProvider extends Component {
  static propTypes = {
    css: PropTypes.instanceOf(Set).isRequired
  }
  static childContextTypes = {
    insertCss: PropTypes.func,
  }

  insertCss = (...styles) => {
    styles.forEach(style => {
      if (typeof style._getCss !== 'function') {
        return
      }

      this.props.css.add(style._getCss())
    })
  }

  getChildContext () {
    return {
      insertCss: this.insertCss
    }
  }

  render () {
    return Children.only(this.props.children)
  }
}
