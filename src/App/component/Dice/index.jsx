import React, { Component } from 'react'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/Dice/index.scss'
import PropTypes from 'prop-types'

class Dice extends Component {
  static propTypes = {
    number: PropTypes.number.isRequired,
  }

  render () {
    const {number} = this.props

    return (
      <div className={style.container}>
        <div className={`${(number === 0) ? style.dice : style['dice' + number]}`}>{this.props.number}</div>
      </div>
    )
  }
}

export default withStyles(style)(Dice)
