import React, {Component} from 'react'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import globalStyle from 'src/assets/Stylesheet/main.scss'

class Layout extends Component {

  scrollToTop = () => {
    if (this.page) this.page.scrollIntoView()
  }

  componentDidMount() {
    this.scrollToTop()
  }

  componentDidUpdate () {
    this.scrollToTop()
  }

  render() {
    return (
      <div className="page__container" ref={page => this.page = page}>
        {this.props.children}
      </div>
    )
  }
}

export default withStyles(globalStyle)(Layout)
