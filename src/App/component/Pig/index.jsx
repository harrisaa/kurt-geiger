import React, { Component } from 'react'
import PropTypes from 'prop-types'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/component/Pig/index.scss'

import Dice from 'src/App/component/Dice'

import { game } from 'src/defaults'

import ButtonPanel from 'src/App/component/ButtonPanel'

export class Pig extends Component {
  static propTypes = {
    updateMessage: PropTypes.func.isRequired,
    playerOneName: PropTypes.string.isRequired,
    playerTwoName: PropTypes.string.isRequired,
  }

  state = {
    currentPlayer: this.props.playerOneName || null,
    playerOneScore: game.zero,
    playerTwoScore: game.zero,
    currentScore: game.zero,
    diceOne: game.zero,
    diceTwo: game.zero,
  }

  componentWillMount () {
    this.setState({currentPlayer: this.props.playerOneName})
  }

  getRandom1To6 = () => {
    return Math.floor(Math.random() * 6) + 1
  }

  rollDice = () => {
    const diceOne = this.getRandom1To6()
    const diceTwo = this.getRandom1To6()
    const score = this.state.currentScore

    this.setState({diceOne, diceTwo, currentScore: score}, () => {
      this.updateScore()
    })
  }

  passTurn = () => {
    const {playerOneName, playerTwoName} = this.props
    const {currentPlayer, playerOneScore, playerTwoScore, currentScore} = this.state
    const score1 = playerOneScore + currentScore
    const score2 = playerTwoScore + currentScore

    if (currentPlayer === playerOneName) {
      this.setState({
        playerOneScore: score1,
        currentPlayer: playerTwoName,
        currentScore: game.zero
      })
    } else {
      this.setState({
        playerTwoScore: score2,
        currentPlayer: playerOneName,
        currentScore: game.zero
      })
    }
  }

  updateScore = () => {
    const {playerOneName, updateMessage} = this.props
    const {currentScore, currentPlayer, diceOne, diceTwo} = this.state
    const currentUser = currentPlayer === playerOneName ? 'playerOne' : 'playerOne'
    const score = currentScore + diceOne + diceTwo
    const currentUserScore = this.state[currentUser + 'Score']
    this.setState({currentScore: score})

    let message = null

    if (diceOne === 1 && diceTwo === 1) {
      message = `Sorry ${currentPlayer}, you have rolled snake eyes and have lost all your points!`

      if (currentPlayer === playerOneName) {
        this.setState({playerOneScore: game.zero, currentScore: game.zero}, () => {
          this.passTurn()
        })
      } else {
        this.setState({playerTwoScore: game.zero, currentScore: game.zero}, () => {
          this.passTurn()
        })
      }
    } else if (diceOne === 1 || diceTwo === 1) {
      message = `Sorry ${currentPlayer}, you have rolled a one and have lost all the points you have just rolled!`
      this.setState({currentScore: game.zero}, () => {
        this.passTurn()
      })
    }

    if (currentUserScore + score >= 100) {
      message = `Congratulations ${currentPlayer}, you have won!`

      if (currentPlayer === playerOneName) {
        this.setState({playerOneScore: currentUserScore + score})
      } else {
        this.setState({playerTwoScore: currentUserScore + score})
      }
    }

    updateMessage( message)
  }

  render () {
    const {playerOneName, playerTwoName} = this.props
    const {playerOneScore, playerTwoScore, currentScore, currentPlayer, diceOne, diceTwo} = this.state
    console.log(diceOne, diceTwo)

    return (
      <div className={style.container}>
        <div className={style.panel}>
          <div>
            <h2
              className={`${style.header} + ' ' + ${(playerOneName === currentPlayer) ? style.current : ''}`}>{playerOneName}</h2>
            <p><strong>Score:</strong> {playerOneScore}</p>
          </div>
          <div>
            <h2
              className={`${style.header} + ' ' + ${(playerTwoName === currentPlayer) ? style.current : ''}`}>{playerTwoName}</h2>
            <p><strong>Score:</strong> {playerTwoScore}</p>
          </div>
        </div>

        <div className={style.panel}>
          <Dice number={diceOne}/>
          <Dice number={diceTwo}/>
        </div>

        <div>
          <p>Current Score: {currentScore}</p>
          <ButtonPanel passTurn={this.passTurn} rollDice={this.rollDice}/>
        </div>
      </div>
    )
  }
}

export default withStyles(style)(Pig)
