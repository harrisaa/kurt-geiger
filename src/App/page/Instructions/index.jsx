import React, { Component } from 'react'

import PigInstructions from 'src/App/component/PigInstructions'

export default class Instructions extends Component {
  render () {

    return (
      <div className="main__container">
        <h1>Pig Instructions</h1>
        <PigInstructions />
      </div>
    )
  }
}
