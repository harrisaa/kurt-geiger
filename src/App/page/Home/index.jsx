import React, { Component } from 'react'
import Game from 'src/App/container/Game'

export default class Home extends Component {

  render () {

    return (
      <div className="main__container">
        <Game />
      </div>
    )
  }
}
