import React, { Component } from 'react'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/page/PageNotFound/index.scss'

export class PageNotFound extends Component {
  render () {
    return (
      <div className={style.main}>
        <h1 className={style.head}>
         Sorry!
          <br />Page Not Found
        </h1>
      </div>
    )
  }
}

export default withStyles(style)(PageNotFound)
