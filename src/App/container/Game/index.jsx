import React, { Component } from 'react'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/container/Game/index.scss'

import Form from 'src/App/container/Form'
import Pig from 'src/App/component/Pig'

const initialState = {
  playerOneName: null,
  playerTwoName: null,
  message: null,
  showForm: true,
}

class Game extends Component {
  state = initialState

  formButton = event => {
    event.preventDefault()
    const {playerOneName, playerTwoName} = this.state
    this.setState({...initialState, playerOneName, playerTwoName, showForm: false})
  }

  resetForm = () => {
    const {playerOneName, playerTwoName} = this.state
    this.setState({...initialState, playerOneName, playerTwoName, showForm: true})
  }

  startGame = () => {
    const {playerOneName, playerTwoName} = this.state
    if (playerOneName && playerTwoName) {
      this.setState({showForm: false})
    }
  }

  onChange = event => {
    const field = event.target
    this.setState({[field.id]: field.value})
  }

  updateMessage = text => {
    this.setState({message: text})
  }

  render () {
    const {playerOneName, playerTwoName, message, showForm} = this.state

    return (
      <React.Fragment>
        <h1 className={style.header}>Pig Game</h1>

        {message && <div className={style.message}><p>{message}</p></div>}

        {showForm && <Form showForm={showForm} startGame={this.startGame} onChange={this.onChange}/>}

        {!showForm &&
        <Pig updateMessage={this.updateMessage} playerOneName={playerOneName} playerTwoName={playerTwoName}/>}

      </React.Fragment>
    )
  }
}

export default withStyles(style)(Game)
