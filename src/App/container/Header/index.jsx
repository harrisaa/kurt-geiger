import {compose} from 'redux'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {graphql} from 'react-apollo/'

import {logOut} from 'src/App/action'
import {logout} from 'src/App/query'

import Header from 'src/App/component/Header'

const mapDispatchToProps = {logOut}

const mergeProps = (stateProps, dispatchProps, ownProps) => ({
  ...stateProps,
  ...dispatchProps,
  ...ownProps,
  logOut: () => {
    // Logout with GraphQL then logout with redux
    return ownProps.logOut().then(() => dispatchProps.logOut())
  },
})

export default compose(
  withRouter,
  graphql(logout, {
    props: ({mutate}) => ({
      logOut: () => mutate()
    })
  }),
  connect(null, mapDispatchToProps, mergeProps)
)(Header)
