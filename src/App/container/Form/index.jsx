import React, { Component } from 'react'

import PropTypes from 'prop-types'

import withStyles from 'isomorphic-style-loader/lib/withStyles'
import style from 'src/App/container/Form/index.scss'

import Username from 'src/App/component/Username'

export class Form extends Component {
  static propTypes = {
    showForm: PropTypes.bool.isRequired,
    startGame: PropTypes.func.isRequired,
    onChange: PropTypes.func.isRequired,
  }

  render () {
    const {showForm, startGame, onChange} = this.props

    return (
      <form>
        {showForm && <Username onChange={onChange}/>}

        <button id="start" onClick={startGame} aria-disabled="true" className={style.play}>Start Game</button>
      </form>
    )
  }
}

export default withStyles(style)(Form)
