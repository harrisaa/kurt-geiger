import React from 'react'
import ReactDOM from 'react-dom'

import { BrowserRouter } from 'react-router-dom'
import { AppContainer } from 'react-hot-loader'

import App from 'src/App'

const css = new Set()
const setupVars = {css}

const render = Component => {
  ReactDOM.hydrate(
    <AppContainer>
      <BrowserRouter>
        <Component {...setupVars}/>
      </BrowserRouter>
    </AppContainer>,
    document.getElementById('root'),
  )
}
render(App)

if (module.hot) {
  module.hot.accept('src/App', () => {
    render(App)
  })
}

