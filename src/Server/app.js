import express from 'express'
import logger from 'morgan'
import path from 'path'
import bodyParser from 'body-parser'
import hbs from 'hbs'

import routes from 'src/Server/routes'
import { env } from 'src/Server/shared'

const app = express()
console.log('env.appPath', env.appPath)

app.set('views', path.join(env.appPath, 'static'))
app.set('view engine', 'html')
app.engine('html', hbs.__express)

if (env.isDev) {
  app.use('/static', express.static(path.join(env.appPath, 'static')))
}

app.use(bodyParser.urlencoded({extended: true}))

// Setup logger
app.use(logger('combined'))
app.use(routes(logger))

routes(app)

export default app
