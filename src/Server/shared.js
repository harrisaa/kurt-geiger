import path from 'path'

const isDev = process.env.NODE_ENV === 'development'

export const env = {
  isDev: isDev,
  appPath: isDev ? path.join(__dirname, '../../build') : path.resolve('./')
}
