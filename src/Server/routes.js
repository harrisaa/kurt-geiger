import React from 'react'

import { AppContainer } from 'react-hot-loader'
import { renderToString } from 'react-dom/server'
import { StaticRouter } from 'react-router'

import App from 'src/App'
import Error from 'src/App/component/Error'

export default () => (req, res) => {

  const css = new Set()
  const setupVars = { css }

  let markup = ''
  try {
    markup = renderToString(
      <AppContainer>
        <StaticRouter location={req.url} context={{}}>
          <App {...setupVars} />
        </StaticRouter>
      </AppContainer>
    )
  } catch (error) {
    markup = renderToString(<Error/>)
  }

  res.render('index', {
    markup,
    css: [...css].join('')
  })
}
