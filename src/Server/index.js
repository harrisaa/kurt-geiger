import http from 'http'

import app from './app'

const PORT = process.env.PORT || 9035

/* eslint no-console: "off" */
http.createServer(app).listen(PORT, '0.0.0.0', () => {
  console.log(`App listening on port ${PORT}!`)
})
