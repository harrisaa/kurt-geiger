export const defaults = {
  languageCode: 'en-GB',
  hackerNewsIdTopStories: 'https://hacker-news.firebaseio.com/v0/topstories.json',
  hackerNewsItem: 'https://hacker-news.firebaseio.com/v0/item/',
}

export const page = {
  home: {url: '/'},
  instructions: {url: '/instructions'},
  notFound: {url: '/notFound'},
}

export const game = {
  zero: 0,
  dice: [1, 2, 3, 4, 5, 6],
}
