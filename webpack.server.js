const webpack = require('webpack')
const path = require('path')
const fs = require('fs')

const nodeModules = {}
const isDev = process.env.NODE_ENV === 'development'
const mode = process.env.NODE_ENV

fs.readdirSync('node_modules')
  .filter(x => ['.bin'].indexOf(x) === -1)
  .forEach(mod => nodeModules[mod] = 'commonjs ' + mod)

module.exports = {
  mode,
  target: 'node',
  cache: false,
  devtool: 'source-map',
  context: path.resolve(__dirname),
  entry: ['./src/Server/index.js'],
  output: {
    path: path.join(__dirname, '/build'),
    filename: 'server.js'
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx', '.scss'],
    alias: {
      Image: path.resolve(__dirname, 'src/assets/image/'),
      Style: path.resolve(__dirname, 'src/assets/Stylesheet/')
    },
    modules: [
      path.resolve(__dirname),
      'node_modules'
    ]
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          {
            loader: 'isomorphic-style-loader'
          },
          {
            loader: 'css-loader',
            options: {
              sourceMap: true,
              modules: true,
              localIdentName: isDev ? '[folder]__[local]--[hash:base64:5]' : '[hash:base64:5]'
            }
          },
          {
            loader: 'sass-loader',
            options: {
              sourceMap: true
            }
          },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|svg|png|woff2?|jpe?g)?(\?.+)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 100000,
              name: '[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.jsx?$/,
        enforce: 'post',
        exclude: /node_modules/,
        use: ['babel-loader']
      }
    ],
    noParse: /\.min\.js/
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        //NODE_ENV: JSON.stringify(process.env.NODE_ENV)
        NODE_ENV: JSON.stringify(path.resolve(__dirname))
      }
    })
  ],
  optimization: {
    namedModules: true, // NamedModulesPlugin()
    splitChunks: { // CommonsChunkPlugin()
      name: 'vendor',
      minChunks: 2
    },
    noEmitOnErrors: true, // NoEmitOnErrorsPlugin
    concatenateModules: true //ModuleConcatenationPlugin
  },
  externals: nodeModules,
  node: {
    __dirname: true,
    fs: 'empty'
  }
}
