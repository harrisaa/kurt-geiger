const webpack = require('webpack')
const config = require('./webpack.server.js')

config.cache = true

config.entry.unshift('webpack/hot/poll?1000')

config.plugins = [
  new webpack.LoaderOptionsPlugin({debug: true}),
  new webpack.HotModuleReplacementPlugin()
]

module.exports = config
