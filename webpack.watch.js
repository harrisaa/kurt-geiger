const config = require('./webpack.config.dev.js')

const hostname = process.env.HOSTNAME || '0.0.0.0'

const port = 9000
const host = `http://${hostname}:${port}/`

config.entry.unshift(
  `webpack-dev-server/client?${host}`,
  'webpack/hot/only-dev-server'
)

config.output.publicPath = `${host}build/`

config.devServer = {
  contentBase: './src',
  disableHostCheck: true,
  historyApiFallback: true,
  hot: true,
  inline: true,
  host,
  stats: {colors: true},
  port: port,
  headers: {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
    'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization'
  },
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  }
}

module.exports = config
