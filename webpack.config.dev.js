const config = require('./webpack.common.js')
const postcssNormalize = require('postcss-normalize')

config.mode = 'development'

config.module = {
  rules: [
    {
      test: /\.scss$/,
      use: [
        {
          loader: 'style-loader'
        },
        {
          loader: 'css-loader',
          options: {
            sourceMap: true,
            modules: true,
            localIdentName: '[folder]__[local]--[hash:base64:5]'
          }
        },
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: true,
            plugins: () => [
              postcssNormalize(/* pluginOptions */)
            ]
          }
        },
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true
          }
        }
      ]
    },
    {
      test: /\.(ttf|eot|svg|png|woff2?|jpe?g)?(\?.+)?$/,
      use: [
        {
          loader: 'url-loader',
          options: {
            limit: 100000,
            name: '[name].[ext]'
          }
        }
      ]
    },
    {
      test: /\.jsx?$/,
      enforce: 'post',
      exclude: /node_modules/,
      use: {
        loader: 'babel-loader',
        options: {
          presets: [
            ['@babel/preset-env'],
            ['@babel/preset-react', {'development': true}]
          ]
        }
      }
    }
  ],
  noParse: /\.min\.js/
}

module.exports = config
